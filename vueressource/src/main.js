// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
var Vue = require('vue');
var VueResource = require('vue-resource');
import App from './App'

/* eslint-disable no-new */

Vue.use(VueResource);
Vue.http.options.root = 'http://jsonplaceholder.typicode.com'
Vue.http.headers.common['monHeader'] = 'toto'
Vue.http.interceptors.push((request, next) => {

    next((response) => {

        if (request.after) {
            request.after.call(this, response);
        }
    });
});

new Vue({
    el: '#app',
    template: '<App>',
    components: {App}
});
