// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import PageA from './components/PageA.vue'
import PageB from './components/PageB.vue'
import PageC from './components/PageC.vue'
import Hello from './components/Hello.vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            // POur la route /LAZY
            // cette methode permet de faire du lazy loading
            // resolve envoi le nom du composant a charger
            // a webpack et celui le charge au moment de l'appel
            // et non au demarrage de l'app
            path: '/lazy',
            component: resolve => require(['./components/PageD.vue'], resolve),
            name: 'PageD'
        },
        {
            path: '/toto',
            component: PageA,
            name: 'namePageA',
            // Meme utilisation avec beforeRouteEnter dans le component voir PageA.vue -- deux methodes
            // beforeEnter(to, from, next){
            //     let confirm = window.confirm('are u toto?')
            //     if (confirm) {
            //         next()
            //     } else {
            //         next('/')
            //     }
            // }
        },
        {
            path: '/a',
            component: PageA,
            name: 'a',
            children: [   // ajouter <router-view> dans PageA
                // vu que celui a des enfants
                {
                    path: 'b',
                    component: PageB,
                    name: 'a.b'
                },
                {
                    path: 'c',
                    component: PageC,
                    name: 'a.c'
                }
            ]
        },
        {
            path: '/tata',
            component: PageB
        },
        {
            path: '/hello',
            component: Hello,
            name: 'nameHello'
        },
        {
            path: '/article/:id(\\d+)',
            components:   // default => router-view &
            // sidebar => router-view name="sidebar"  attention au S de componentS
                {
                    default: PageB,
                    sidebar: PageC
                },
            name: 'article'
        },
        {
            path: '*',
            redirect: '/hello'
        }
    ]
})
new Vue({
    el: '#app',
    router: router,
    template: '<App/>',
    components: {App}
})
